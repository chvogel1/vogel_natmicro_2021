#!/bin/bash

# script to annotate bacterial genome assembly with Prokka.


# exits whenever a function returns 1-
set -e
# exits if unset variables are used
set -o nounset

# parse arguments
config_file=$1

if [ ! -f $config_file ]
then
echo "invalid config file"
    return 1
fi

# load config file
source $config_file

log() {

        echo $(date -u)": "$1 >> $logfile

}
# modules
module load prokka

for genome_id in $(cut -f 2 $mapping_file | tail -n +2)
do

    log "["$genome_id"] genome annotation using prokka..."
	
	
	prokka  --outdir $genome_id --prefix $genome_id $working_dir/Genomes/"$genome_id"_scaffolds.fasta
	
		
	log "["$genome_id"] done"

done

log "DONE!"
