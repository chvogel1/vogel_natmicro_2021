Scripts to assemble and annotate genomes of bacterial leaf isolates. 

1. Quality control and processing of reads. Then assembly of clean and downsampled reads with SPAdes. 
./SPAdes_assembly.sh config_file_xx.sh 

2. Filtering of SPAdes assembly by coverage
./SPAdes_assembly_filter.sh config_file_xx.sh

3. Annotation with Prokka
./prokka_annotation.sh config_file_xx.sh


with xx: May2017 or Sep2017 or October2017