#!/bin/bash

# functions for filtering assembly prepared by SPAdes
#
# by Christine Vogel (chvogel@ethz.ch)


log() {

    echo $(date -u)": "$1 >> $logfile

}

config () {

    # parse arguments
    local config_file=$1

	# check config file
	if [ ! -f $config_file ]
	then
	    echo "invalid config file"
	    return 1
	fi
	
	# get dir and file paths
	source $config_file

}


SPAdes_postfilter_contigs () {
 	
    # parse arguments
	local minc=100
	local minp=95
	local minr=10
	local minl=500
	local trim=0
	local ow="f"
	local app="f"
	local minhits=2
	local maxindel=0
	local tipsearch=0
	local bw=20
	local rescue="f"
	local threads=16
	
    for i in "$@"
    do
        case $i in
			-genome_id=* )
			genome_id=`echo $i | sed 's/^.genome_id.//g'` ;;
			-ow=* )
			ow=`echo $i | sed 's/^.ow.//g'` ;;
			-app=* )
			app=`echo $i | sed 's/^.app.//g'` ;;
			-minhits=* )
			minhits=`echo $i | sed 's/^.minhits.//g'` ;;
			-maxindel=* )
			maxindel=`echo $i | sed 's/^.maxindel.//g'` ;;
			-tipsearch=* )
			tipsearch=`echo $i | sed 's/^.tipsearch.//g'` ;;
			-bw=* )
			bw=`echo $i | sed 's/^.bw.//g'` ;;
			-rescue=* )
			rescue=`echo $i | sed 's/^.rescue.//g'` ;;			
			-minc=* )
			minc=`echo $i | sed 's/^.minc.//g'` ;;
			-minp=* )
			minp=`echo $i | sed 's/^.minp.//g'` ;;
			-minr=* )
			minr=`echo $i | sed 's/^.minr.//g'` ;;	
			-minl=* )
			minl=`echo $i | sed 's/^.minl.//g'` ;;	
			-trim=* )
			trim=`echo $i | sed 's/^.trim.//g'` ;;	
		esac
    done
    
    #~ # prepare folder for assembly file

    
    
	#local dir=$PWD
	
	local filter_dir=$working_dir/"$genome_id"
	
	
	cd $filter_dir
    # run the filter
	
	postfilter.sh threads=$threads in=$filter_dir/"$genome_id"_R1_clean.fastq.gz in2=$filter_dir/"$genome_id"_R2_clean.fastq.gz \
	ref=$filter_dir/contigs.fasta cov=$filter_dir/"$genome_id"_covstats_contigs.txt out=$filter_dir/"$genome_id"_contigs_filtered.fasta \
	minc=$minc minl=$minl minr=$minr minp=$minp minhits=$minhits maxindel=$maxindel tipsearch=$tipsearch trim=$trim outdirty=$filter_dir/"$genome_id"_removed_contigs.fasta \
	#ow=$ow app=$app bw=$bw rescue=$rescue 

	#cd dir
}

SPAdes_postfilter_scaffolds () {
 	
    # parse arguments
	local minc=100
	local minp=95
	local minr=10
	local minl=500
	local trim=0
	local ow="f"
	local app="f"
	local minhits=2
	local maxindel=0
	local tipsearch=0
	local bw=20
	local rescue="f"
	local threads=16
	
    for i in "$@"
    do
        case $i in
			-genome_id=* )
			genome_id=`echo $i | sed 's/^.genome_id.//g'` ;;
			-ow=* )
			ow=`echo $i | sed 's/^.ow.//g'` ;;
			-app=* )
			app=`echo $i | sed 's/^.app.//g'` ;;
			-minhits=* )
			minhits=`echo $i | sed 's/^.minhits.//g'` ;;
			-maxindel=* )
			maxindel=`echo $i | sed 's/^.maxindel.//g'` ;;
			-tipsearch=* )
			tipsearch=`echo $i | sed 's/^.tipsearch.//g'` ;;
			-bw=* )
			bw=`echo $i | sed 's/^.bw.//g'` ;;
			-rescue=* )
			rescue=`echo $i | sed 's/^.rescue.//g'` ;;			
			-minc=* )
			minc=`echo $i | sed 's/^.minc.//g'` ;;
			-minp=* )
			minp=`echo $i | sed 's/^.minp.//g'` ;;
			-minr=* )
			minr=`echo $i | sed 's/^.minr.//g'` ;;	
			-minl=* )
			minl=`echo $i | sed 's/^.minl.//g'` ;;	
			-trim=* )
			trim=`echo $i | sed 's/^.trim.//g'` ;;	
		esac
    done
    
    #~ # prepare folder for assembly file

    
    
	#local dir=$PWD
	
	local filter_dir=$working_dir/"$genome_id"
	
	
	cd $filter_dir
    # run the filter
	
	postfilter.sh threads=$threads in=$filter_dir/"$genome_id"_R1_clean.fastq.gz in2=$filter_dir/"$genome_id"_R2_clean.fastq.gz \
	ref=$filter_dir/scaffolds.fasta cov=$filter_dir/"$genome_id"_covstats_scaffolds.txt out=$filter_dir/"$genome_id"_scaffolds_filtered.fasta \
	minc=$minc minl=$minl minr=$minr minp=$minp minhits=$minhits maxindel=$maxindel tipsearch=$tipsearch trim=$trim outdirty=$filter_dir/"$genome_id"_removed_scaffolds.fasta \
	#ow=$ow app=$app bw=$bw rescue=$rescue 

	#cd dir
}