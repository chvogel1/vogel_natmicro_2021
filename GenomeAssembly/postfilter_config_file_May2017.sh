#!/bin/bash

# Paths for SPAdes_assembly_filter.sh script. 


# paths

# folder containing raw reads
data_dir="/gram/vorholt/chvogel/OMICS/At-SPHERE/Genomes/May2017/"
# output and intermediate data folder
working_dir="/nfs/home/chvogel/working_dir/Postfilter"
# mapping file with isolates data
mapping_file="$data_dir/May2017_mapping_file.txt"

# debug
logfile=""$working_dir"/logfile_May2017_filter.txt"
