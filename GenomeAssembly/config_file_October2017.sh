#!/bin/bash

# File containing paths and parameters for genome assembly using SPAdes. The script was written by Christine Vogel (chvogel@ethz.ch) 
# Adapted from scripts written by Ruben Garrido-Oter (garridoo@mpipz.mpg.de)and published in Bai et al. 2015 (https://doi.org/10.1038/nature16192)

# paths

# folder containing raw reads
raw_data_dir="/gram/shared/chvogel/NRP72/Genomes/unpublished/October2017/Raw_reads"
# intermediate data folder
data_dir="/gram/shared/chvogel/NRP72/Genomes/unpublished/October2017"
# output and intermediate data folder
working_dir="/nas/chvogel/working_dir"
# mapping file with isolates data
mapping_file="$data_dir/October2017_mapping_file.txt"

# debug
logfile="/dev/tty"
output=""$working_dir"/output.txt"

#parameters BBDuk
threads=3

# parameters (SPAdes)
cov_cutoff="auto"

