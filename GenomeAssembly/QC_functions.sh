#!/bin/bash

# functions for trimming of raw Illumina HiSeq reads and QC with FastQC 
#
# by Christine Vogel (chvogel@ethz.ch)


log() {

    echo $(date -u)": "$1 >> $logfile

}

config () {

    # parse arguments
    local config_file=$1

	# check config file
	if [ ! -f $config_file ]
	then
	    echo "invalid config file"
	    return 1
	fi
	
	# get dir and file paths
	source $config_file

}


qc_sequencing () {
	# parse arguments
    for i in "$@"
    do
	# sed: stream editor: -> all the input passes through and goes to STDOUT and does not change the input file
	# s/pattern1/pattern2/ -> replaces first occurrence of pattern1 with pattern2; g at the end -> global substitution (not just the first occurrence)
	# ^ matches the beginning of lines, . matches any single character
	
        case $i in
            -genome_id=* )
            genome_id=`echo $i | sed 's/^.genome_id.//g'` ;;
			
        esac
    done
	
	#~ # prepare folder for FastQC results

    log "[genome "$genome_id"] initializing FastQC directory..."
    
    local QC_dir="$working_dir"/FastQC/"$genome_id"
    if [ -d "$QC_dir" ]
    then
        local ndatfiles=$(ls -A "$QC_dir" | grep $genome_id | wc -l)ls 
        if [ $ndatfiles -gt 0 ]
        then
            log "[genome "$genome_id"] QC folder is not empty!"
            return 1
        fi
    else
        mkdir -p $QC_dir
    fi
    
    # run the sequencing quality control

    log "[genome "$genome_id"] running FastQC sequencing quality control..."
    
    local dir=$PWD

    cd $QC_dir

    fastqc -o $QC_dir $working_dir/"$genome_id"_R1.fastq \
				$working_dir/"$genome_id"_R2.fastq \
                -o $QC_dir
				

	cd $dir
	
}


trim_bbduk_CV () {

    # parse arguments
    local threads=1
	
	for i in "$@"
    do
	
	    case $i in
            -genome_id=* )
            genome_id=`echo $i | sed 's/^.genome_id.//g'` ;;
            -threads=* )
            threads=`echo $i | sed 's/^.threads.//g'` ;;			
           
        esac
    done
    
    if [[ (! -f $working_dir/"$genome_id"_R1.fastq.gz || ! -f $working_dir/"$genome_id"_R2.fastq.gz) ]]
    then
        log "[genome "$genome_id"] fastq.gz file not found!"
        return 1
    fi

    # prepare folder for BBduk_trim results

    log "[genome "$genome_id"] initializing trimming directory..."
    
    local Trim_dir="$working_dir"/BBDuk/"$genome_id"
    if [ -d "$Trim_dir" ]
    then
        local ndatfiles=$(ls -A "$Trim_dir" | grep $genome_id | wc -l)ls 
        if [ $ndatfiles -gt 0 ]
        then
            log "[genome "$genome_id"] Trim folder is not empty!"
            return 1
        fi
    else
        mkdir -p $Trim_dir
    fi
    
    # run the trimming pipeline of BBDuk, adapted from Shini's pipeline

    log "[genome "$genome_id"] trimming raw reads using BBDuk..."
    
    local dir=$PWD

    cd $Trim_dir

    #with bbduk first remove the extra base in case there is one (so 151 bp to 150 bp, 251 bp to 250 bp), then the adapters, then small contaminants and in the end do quality trimming -> 3 times call to bbduk.sh
	#note: outm -> output that didn't meet the criteria; out -> output tht met the criteria. 
	#
	
	bbduk.sh -Xmx1G usejni=t threads=$threads overwrite=t \
		in1=$working_dir/"$genome_id"_R1.fastq.gz in2=$working_dir/"$genome_id"_R2.fastq.gz \
		ref=/opt/apps/software/BBMap/37.56-foss-2016b-Java-1.8.0_144/resources/adapters.fa \
		ktrim=r k=23 mink=11 hdist=1 tpe tbo ftm=5 out=stdout.fq \
		outm=$Trim_dir/"$genome_id"_output.adapter_matched outs=$Trim_dir/"$genome_id"_output.adapter_singletons \
		refstats=$Trim_dir/"$genome_id"_output.adapter_stats statscolumns=5 \
		2>> $Trim_dir/"$genome_id"_log.logfile| bbduk.sh -Xmx1G usejni=t \
		threads=$threads interleaved=true overwrite=t in=stdin.fq out=stdout.fq \
		outm=$Trim_dir/"$genome_id"_output.phix_matched outs=$Trim_dir/"$genome_id"_output.phix_singletons \
		ref=/opt/apps/software/BBMap/37.56-foss-2016b-Java-1.8.0_144/resources/phix_adapters.fa.gz \
		k=31 hdist=1 refstats=$Trim_dir/"$genome_id"_output.phix_stats statscolumns=5 \
		2>> $Trim_dir/"$genome_id"_log.logfile| bbduk.sh -Xmx1G usejni=t \
		threads=$threads overwrite=t interleaved=true in=stdin.fq fastawrap=10000 interleaved=true \
		outm=$Trim_dir/"$genome_id"_output.qc_failed outs=$Trim_dir/"$genome_id"_output.qc_singletons.fastq.gz minlength=30 \
		qtrim=rl maq=20 maxns=0 overwrite=t stats=$Trim_dir/"$genome_id"_output.qc_stats statscolumns=5 \
		trimq=14 qtrim=rl out=$Trim_dir/"$genome_id"_R1_clean.fastq.gz out2=$Trim_dir/"$genome_id"_R2_clean.fastq.gz
		2>> "$genome_id"_log.logfile
		
	cd $dir

}

qc_post_trim () {
	# parse arguments
    # sed: stream editor: -> all the input passes through and goes to STDOUT and does not change the input file
	# s/pattern1/pattern2/ -> replaces first occurrence of pattern1 with pattern2; g at the end -> global substitution (not just the first occurrence)
	# ^ matches the beginning of lines, . matches any single character
	for i in "$@"
    do
	    case $i in
            -genome_id=* )
            genome_id=`echo $i | sed 's/^.genome_id.//g'` ;;
			
        esac
    done
	
	#~ # prepare folder for FastQC results

    log "[genome "$genome_id"] initializing FastQC directory..."
    
    local QC_dir="$working_dir"/FastQC_post_trim/"$genome_id"
    if [ -d "$QC_dir" ]
    then
        local ndatfiles=$(ls -A "$QC_dir" | grep $genome_id | wc -l)ls 
        if [ $ndatfiles -gt 0 ]
        then
            log "[genome "$genome_id"] QC folder is not empty!"
            return 1
        fi
    else
        mkdir -p $QC_dir
    fi
    
    # run the sequencing quality control

    log "[genome "$genome_id"] running FastQC sequencing quality control of BBDuk trimmed reads..."
    
    local dir=$PWD

    cd $QC_dir

    fastqc -o $QC_dir $working_dir/BBDuk/"$genome_id"/"$genome_id"_R1_clean.fastq.gz \
		$working_dir/BBDuk/"$genome_id"/"$genome_id"_R2_clean.fastq.gz \

	cd $dir
	
}

norm_bbnorm_CV () {

    # parse arguments
    local threads=1
	local target=100
	
	for i in "$@"
    do
	
	    case $i in
            -genome_id=* )
            genome_id=`echo $i | sed 's/^.genome_id.//g'` ;;
            -threads=* )
            threads=`echo $i | sed 's/^.threads.//g'` ;;	
			-target=* )
            target=`echo $i | sed 's/^.target.//g'` ;;
           
        esac
    done
    
    if [[ (! -f $working_dir/BBDuk/"$genome_id"/"$genome_id"_R1_clean.fastq.gz || ! -f $working_dir/BBDuk/"$genome_id"/"$genome_id"_R2_clean.fastq.gz) ]]
    then
        log "[genome "$genome_id"] clean_fastq.gz file not found!"
        return 1
    fi

    # prepare folder for BBduk_norm results

    log "[genome "$genome_id"] initializing normalizing directory..."
    
    local Norm_dir="$working_dir"/BBNorm/"$genome_id"
    if [ -d "$Norm_dir" ]
    then
        local ndatfiles=$(ls -A "$Norm_dir" | grep $genome_id | wc -l)ls 
        if [ $ndatfiles -gt 0 ]
        then
            log "[genome "$genome_id"] Normalizing folder is not empty!"
            return 1
        fi
    else
        mkdir -p $Norm_dir
    fi
    
    # run the normalization with bbnorm.sh from BBMap

    log "[genome "$genome_id"] normalizing clean reads using BBNorm..."
    
    local dir=$PWD

    cd $Norm_dir

    #with bbnorm
		
	bbnorm.sh -Xmx40g \
		in1=$working_dir/BBDuk/"$genome_id"/"$genome_id"_R1_clean.fastq.gz in2=$working_dir/BBDuk/"$genome_id"/"$genome_id"_R2_clean.fastq.gz \
		out1=$working_dir/"$genome_id"_R1_norm.fastq.gz out2=$working_dir/"$genome_id"_R2_norm.fastq.gz \
		target=$target threads=$threads overwrite=t 2>> $Norm_dir/"$genome_id"_log.logfile
	
	cd $dir

}

assemblySPAdes_bbduk () {
    
    # parse arguments
	local cov_cutoff="auto"
	local threads=1
	
	
    for i in "$@"
    do
        case $i in
			-genome_id=* )
			genome_id=`echo $i | sed 's/^.genome_id.//g'` ;;
			-cov_cutoff=* )
			cov_cutoff=`echo $i | sed 's/^.cov_cutoff.//g'` ;;
			-threads=* )
			threads=`echo $i | sed 's/^.threads.//g'` ;;	
			
        esac
    done
    
    #~ # prepare folder for assembly file

    log "[genome "$genome_id"] initializing assembly directory..."
    
    local assembly_dir="$working_dir"/SPAdes_3_11/"$genome_id"
    if [ -d "$assembly_dir" ]
    then
        local ndatfiles=$(ls -A "$assembly_dir" | grep $genome_id | wc -l)
        if [ $ndatfiles -gt 0 ]
        then
            log "[genome "$genome_id"] assembly folder is not empty!"
            return 1
        fi
    else
        mkdir -p $assembly_dir
    fi
    
    # run the assembler

    log "[genome "$genome_id"] running SPAdes assembler..."
    
    local dir=$PWD

    cd $assembly_dir

    spades.py 	-k 21,33,55,77 --careful --cov-cutoff $cov_cutoff --threads $threads --pe1-1 $working_dir/"$genome_id"_R1_norm.fastq.gz \
				--pe1-2 $working_dir/"$genome_id"_R2_norm.fastq.gz \
                -o $assembly_dir
				
	cp $assembly_dir/contigs.fasta $working_dir/Genomes/"$genome_id"_contigs.fasta
	cp $assembly_dir/scaffolds.fasta $working_dir/Genomes/"$genome_id"_scaffolds.fasta
				

	cd $dir
   
}

quast_quality () {
	
	local threads=1
	
	for i in "$@"
    do
        case $i in
			-genome_id=* )
			genome_id=`echo $i | sed 's/^.genome_id.//g'` ;;
			-threads=* )
			threads=`echo $i | sed 's/^.threads.//g'` ;;	
			
        esac
    done

	#~ # prepare folder for quast file

    log "[genome "$genome_id"] initializing QUAST directory..."
    
    local quast_dir="$working_dir"/quast_results/"$genome_id"
    if [ -d "$quast_dir" ]
    then
        local ndatfiles=$(ls -A "$quast_dir" | grep $genome_id | wc -l)
        if [ $ndatfiles -gt 0 ]
        then
            log "[genome "$genome_id"] assembly folder is not empty!"
            return 1
        fi
    else
        mkdir -p $quast_dir
    fi
	
	#check the quality of the assembly with quast
	quast.py -o $quast_dir --threads $threads $working_dir/Genomes/"$genome_id"_contigs.fasta
	

}


