#!/bin/bash

# Paths and parameters for filtering of SPAdes assembly
# the script was adapted by Christine Vogel (chvogel@ethz.ch) 


# paths

# folder containing raw reads
data_dir="/gram/vorholt/chvogel/OMICS/At-SPHERE/Genomes/May2017/"
# output and intermediate data folder
working_dir="/nfs/home/chvogel/working_dir/Postfilter"
# mapping file with isolates data
mapping_file="$data_dir/20180720_May2017_mapping.txt"

# debug
logfile=""$working_dir"/logfile_May2017_filter.txt"
