#!/bin/bash

# script to filter SPAdes assemblies based on coverage.


# exits whenever a function returns 1-
set -e
# exits if unset variables are used
set -o nounset

# parse arguments
config_file=$1

if [ ! -f $config_file ]
then
echo "invalid config file"
    return 1
fi

source $config_file

# different functions
source "postfilter_functions.sh"
# modules
module load BBMap


for genome_id in $(cut -f 2 $mapping_file | tail -n +2)
do

	log "["$genome_id"] prepare data ..."
	
	mkdir $working_dir/"$genome_id"
	
	cp $data_dir/BBduk/"$genome_id"/"$genome_id"_R1_clean.fastq.gz $working_dir/"$genome_id"
	cp $data_dir/BBduk/"$genome_id"/"$genome_id"_R2_clean.fastq.gz $working_dir/"$genome_id"
	cp $data_dir/SPAdes_3_11/$genome_id/contigs.fasta $working_dir/"$genome_id"/
	cp $data_dir/SPAdes_3_11/$genome_id/scaffolds.fasta $working_dir/"$genome_id"/
	
	log "["$genome_id"] filter the data ..."
	
	SPAdes_postfilter_contigs -genome_id=$genome_id -minc=100 minp=95 minl=200
	
	SPAdes_postfilter_scaffolds -genome_id=$genome_id -minc=100 minp=95 minl2500 
		
	#copy results back to data folder and clean up working environment	
	cp -a $working_dir/"$genome_id"/"$genome_id"_contigs_filtered.fasta $data_dir/SPAdes_3_11/"$genome_id"/
	cp -a $working_dir/"$genome_id"/"$genome_id"_removed_contigs.fasta $data_dir/SPAdes_3_11/"$genome_id"/
	cp -a $working_dir/"$genome_id"/"$genome_id"_covstats_contigs.txt $data_dir/SPAdes_3_11/"$genome_id"/
	
	cp -a $working_dir/"$genome_id"/"$genome_id"_scaffolds_filtered.fasta $data_dir/SPAdes_3_11/"$genome_id"/
	cp -a $working_dir/"$genome_id"/"$genome_id"_removed_scaffolds.fasta $data_dir/SPAdes_3_11/"$genome_id"/
	cp -a $working_dir/"$genome_id"/"$genome_id"_covstats_scaffolds.txt $data_dir/SPAdes_3_11/"$genome_id"/
	
	log "["$genome_id"] done"
	
	
done

log "DONE!"
