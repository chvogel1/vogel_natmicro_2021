#!/bin/bash

# script to assemble the genomes of Leaf isolates from raw paired-end reads.
#
# adapted from script by Ruben Garrido (assembly.sh, Bai et al. 2015, Nature https://doi.org/10.1038/nature16192) by Christine Vogel (chvogel@ethz.ch)


# exits whenever a function returns 1-
set -e
# exits if unset variables are used
set -o nounset

# parse arguments
config_file=$1

if [ ! -f $config_file ]
then
echo "invalid config file"
    return 1
fi

# load config file
source $config_file

# load functions
source "QC_functions.sh"

# modules
module load BBMap
module load FastQC
module load SPAdes/3.11.0-foss-2016b
module load QUAST

for genome_id in $(cut -f 2 $mapping_file | tail -n +2)
do

	log "["$genome_id"] processing raw data..."
	
	cp $raw_data_dir/"$genome_id"_R1.fastq.gz $working_dir/
	cp $raw_data_dir/"$genome_id"_R2.fastq.gz $working_dir/
	

	trim_bbduk_CV -genome_id=$genome_id -threads=$threads \
         &>> $output
	
	qc_post_trim -genome_id=$genome_id \
		&>> $output
	
	#copy results back to NRP72 folder and then cleanup
	cp -a $working_dir/BBDuk/"$genome_id" $data_dir/BBDuk/
	cp -a $working_dir/FastQC_post_trim $data_dir/BBDuk/

	
	rm -f $working_dir/"$genome_id"_*fastq.gz
	rm -f -r $working_dir/FastQC_post_trim/"$genome_id"


    log "["$genome_id"] getting the data for genome assembly and normalize to around 100x using BBnorm..."
	
	norm_bbnorm_CV -genome_id=$genome_id -threads=$threads -target=100
	
	log "["$genome_id"] assembling genome with SPAdes..."

	assemblySPAdes_bbduk -genome_id=$genome_id -cov_cutoff=$cov_cutoff -threads=$threads \
         &>> $output
		 	
	
	#copy results back to NRP72 folder and then cleanup
	cp -a $working_dir/SPAdes_3_11/"$genome_id" $data_dir/SPAdes_3_11/
	cp $working_dir/"$genome_id"_R1_norm.fastq.gz $working_dir/"$genome_id"_R2_norm.fastq.gz $data_dir/BBnorm
	rm -f -r $working_dir/SPAdes_3_11/"$genome_id"
	rm $working_dir/"$genome_id"*
	rm -f -r $working_dir/BBDuk/"$genome_id"
	
	log "["$genome_id"] genome quality control with QUAST..."
	
	quast_quality -genome_id=$genome_id -threads=$threads \
		&>> $output
	
	#copy results back to NRP72 folder and then cleanup	
	mkdir $data_dir/SPAdes_3_11/"$genome_id"/QUAST
	cp -a $working_dir/quast_results/"$genome_id" $data_dir/SPAdes_3_11/"$genome_id"/QUAST
	rm -f -r $working_dir/quast_results/"$genome_id"
	
	log "["$genome_id"] done"

done

log "DONE!"
