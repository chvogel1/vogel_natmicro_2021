Func_id	Func_name	Comment2	Leaf202	Leaf68	Leaf155	Leaf262	Leaf311	Leaf321	Leaf306	Leaf384	Leaf383	Leaf341	Leaf371	Leaf391	Leaf453	Leaf386
COG0075	Archaeal aspartate aminotransferase or a related aminotransferase, includes purine catabolism protein PucG		1	1	1	1	0	0	0	0	0	0	0	0	0	0
COG0076	Glutamate or tyrosine decarboxylase or a related PLP-dependent protein		1	1	2	0	0	0	0	0	0	0	0	0	0	0
COG0168	Trk-type K+ transport system, membrane component	TrkG	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG0252	L-asparaginase/archaeal Glu-tRNAGln amidotransferase subunit D	AnsA: translation, ribosomal structure and biogenesis, intracellular trafficking, secretion and vesicular transport https://www.ncbi.nlm.nih.gov/Structure/cdd/cddsrv.cgi?uid=COG0252	2	2	2	1	1	0	0	0	0	0	0	0	0	0
COG0569	Trk K+ transport system, NAD-binding component	TrkA	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG0754	Glutathionylspermidine synthase		1	1	1	0	0	0	0	0	0	0	0	0	0	0
COG1014	Pyruvate:ferredoxin oxidoreductase or related 2-oxoacid:ferredoxin oxidoreductase, gamma subunit		1	1	1	1	0	0	0	0	0	0	0	0	0	0
COG1027	Aspartate ammonia-lyase	AspA: aspartate to fumarate and ammonia (and reverse). Participates in alanine and aspartate metabolism and nitrogen metabolism	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG1113	L-asparagine transporter and related permeases	amino acid transport	1	1	1	1	2	0	0	0	0	0	0	0	0	0
COG1240	Mg-chelatase subunit ChlD		1	1	1	0	0	0	0	0	0	0	0	0	0	0
COG1300	Uncharacterized membrane protein SpoIIM, required for sporulation		1	1	1	0	0	0	0	0	0	0	0	0	0	0
COG1501	Alpha-glucosidase, glycosyl hydrolase family GH31		1	1	1	0	1	0	0	0	0	0	0	0	0	0
COG1694	NTP pyrophosphatase, house-cleaning of non-canonical NTPs		1	1	1	1	0	0	0	0	0	0	0	0	0	0
COG1708	Predicted nucleotidyltransferase		1	1	2	0	0	0	0	0	0	0	0	0	0	0
COG1858	Cytochrome c peroxidase	MauG, in Paracoocus denitrificans methylamine utilization protein, presumably in step of biosynthesis of tryptophan tryptophylquinone cofactor (posttranslational modification, protein turnover, chaperones)	1	1	1	2	2	0	0	0	0	0	0	0	0	0
COG1872	Uncharacterized conserved protein YggU, UPF0235/DUF167 family		1	1	1	0	0	0	0	0	0	0	0	0	0	0
COG1878	Kynurenine formamidase	"amino acid transport and metabolism (part of cyclase superfamily cl00814); tryptophan degradation. Hydrolyses Try to L-kynurenine and formate http://www.ebi.ac.uk/interpro/entry/InterPro/IPR017484/"	1	1	1	2	1	0	0	0	0	0	0	0	0	0
COG1994	Zn-dependent protease (includes SpoIVFB)		1	1	1	0	0	0	0	0	0	0	0	0	0	0
COG2315	Predicted DNA-binding protein with ?double-wing? structural motif, MmcQ/YjbR family	Transcription	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG2320	GrpB domain, predicted nucleotidyltransferase, UPF0157 family		3	3	1	0	1	0	0	0	0	0	0	0	0	0
COG2345	Predicted transcriptional regulator, ArsR family		1	1	1	0	0	0	0	0	0	0	0	0	0	0
COG2828	2-Methylaconitate cis-trans-isomerase PrpF (2-methyl citrate pathway)	PrpF (energy production and conversion), methyl citrate pathway is for propionate degradation route (catabolism of short chain fatty acids? https://www.uniprot.org/uniprot/Q8EJW4). Propanoate pathway -> organic acid metabolism (auch uiprot)	1	1	1	2	1	0	0	0	0	0	0	0	0	0
COG2855	Uncharacterized membrane protein YadS		1	1	1	0	1	0	0	0	0	0	0	0	0	0
COG3157	Type VI protein secretion system component Hcp (secreted cytotoxin)	T6SS	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG3284	Transcriptional regulator of acetoin/glycerol metabolism		1	1	1	0	0	0	0	0	0	0	0	0	0	0
COG3455	Type VI protein secretion system component VasF	T6SS	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG3456	Predicted component of the type VI protein secretion system, contains a FHA domain	T6SS	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG3501	Uncharacterized conserved protein, implicated in type VI secretion and phage assembly	T6SS	1	1	1	2	4	0	0	0	0	0	0	0	0	0
COG3515	Predicted component of the type VI protein secretion system	T6SS	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG3516	Predicted component of the type VI protein secretion system	T6SS	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG3517	Predicted component of the type VI protein secretion system	T6SS	2	2	2	2	2	0	0	0	0	0	0	0	0	0
COG3518	Predicted component of the type VI protein secretion system	T6SS	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG3519	Type VI protein secretion system component VasA	T6SS	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG3520	Predicted component of the type VI protein secretion system	T6SS	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG3522	Predicted component of the type VI protein secretion system	T6SS	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG3523	Type VI protein secretion system component VasK	T6SS	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG3642	tRNA A-37 threonylcarbamoyl transferase component Bud32		1	1	1	1	0	0	0	0	0	0	0	0	0	0
COG3766	Uncharacterized membrane protein YjfL, UPF0719 family		1	1	1	0	0	0	0	0	0	0	0	0	0	0
COG3772	Phage-related lysozyme (muramidase), GH24 family		1	1	3	0	0	0	0	0	0	0	0	0	0	0
COG3911	Predicted ATPase		1	1	1	0	1	0	0	0	0	0	0	0	0	0
COG3913	Uncharacterized protein	SciT, tagF	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG4119	Predicted NTP pyrophosphohydrolase, NUDIX family		1	1	1	0	0	0	0	0	0	0	0	0	0	0
COG4231	TPP-dependent indolepyruvate ferredoxin oxidoreductase, alpha subunit		1	1	1	1	0	0	0	0	0	0	0	0	0	0
COG4390	Uncharacterized protein		1	1	1	0	0	0	0	0	0	0	0	0	0	0
COG4455	Protein of avirulence locus involved in temperature-dependent protein secretion	ImpE, tagJ	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG4538	Uncharacterized protein		1	1	1	0	1	0	0	0	0	0	0	0	0	0
COG4680	mRNA-degrading endonuclease (mRNA interferase) HigB, toxic component of the HigAB toxin-antitoxin module		1	1	1	1	0	0	0	0	0	0	0	0	0	0
COG5460	Uncharacterized conserved protein, DUF2164 family	0	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG5463	Uncharacterized conserved protein YgiB, involved in bioifilm formation, UPF0441/DUF1190 family		1	1	1	0	0	0	0	0	0	0	0	0	0	0
COG5499	Antitoxin component HigA of the HigAB toxin-antitoxin module, contains an N-terminal HTH domain		1	1	1	1	0	0	0	0	0	0	0	0	0	0
COG5502	Uncharacterized conserved protein, DUF2267 family	0	1	1	1	1	1	0	0	0	0	0	0	0	0	0
COG5528	Uncharacterized membrane protein		1	1	1	1	0	0	0	0	0	0	0	0	0	0
