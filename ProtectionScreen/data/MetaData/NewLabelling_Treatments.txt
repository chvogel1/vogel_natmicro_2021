Strain_ID	TreatmentPart	Remove	Strain_ID.new	AtLSPHERE_main_Genus	Use
15			15	Pseudomonas	TRUE
154			154	Curtobacterium	TRUE
21			21	Sphingomonas	TRUE
C1	Mix3		M3.4	#N/A	TRUE
205			205	Sphingomonas	TRUE
233			233	Rhodococcus	TRUE
61_cont		Remove_cont	61_cont	#N/A	FALSE
C2	Mix3	Remove_cont	C2	#N/A	FALSE
Ax			Ax	#N/A	TRUE
Fr1			Fr1	#N/A	TRUE
M	Mix10		M10.1	#N/A	TRUE
R1	Mix10		M10.2	#N/A	TRUE
R2	Mix10		M10.3	#N/A	TRUE
R3	Mix10		M10.4	#N/A	TRUE
R4	Mix10		M10.5	#N/A	TRUE
R5	Mix10		M10.6	#N/A	TRUE
R6	Mix10		M10.7	#N/A	TRUE
183			183	Curtobacterium	TRUE
48			48	Pseudomonas	TRUE
C4	Mix3		M3.1	#N/A	TRUE
179			179	Microbacterium	TRUE
453			453	Rhizobium	TRUE
67			67	Sphingomonas	TRUE
C5	Mix3		M3.2	#N/A	TRUE
225			225	Rhodococcus	TRUE
347			347	Microbacterium	TRUE
371			371	Rhizobium	TRUE
C6	Mix3		M3.3	#N/A	TRUE
111			111	Methylobacterium	TRUE
118			118	Methylobacterium	TRUE
202			202	Rhizobium	TRUE
208			208	Sphingomonas	TRUE
245			245	Aeromicrobium	TRUE
28			28	Sphingomonas	TRUE
405			405	Chryseobacterium	TRUE
72			72	Paenibacillus	TRUE
91			91	Methylobacterium	TRUE
R10	Mix10	Remove_onlyCombi2	R10	#N/A	FALSE
R11	Mix10		M10.48	#N/A	TRUE
R12	Mix10		M10.49	#N/A	TRUE
R7	Mix10	Remove_onlyCombi2	R7	#N/A	FALSE
R8	Mix10	Remove_onlyCombi2	R8	#N/A	FALSE
R9	Mix10		M10.50	#N/A	TRUE
M1	Mix10		M10.51	#N/A	TRUE
M10	Mix10		M10.8	#N/A	TRUE
M11	Mix10		M10.9	#N/A	TRUE
M12	Mix10		M10.10	#N/A	TRUE
M13	Mix10		M10.11	#N/A	TRUE
M14	Mix10		M10.12	#N/A	TRUE
M15	Mix10		M10.13	#N/A	TRUE
M16	Mix10		M10.14	#N/A	TRUE
M18	Mix10		M10.15	#N/A	TRUE
M19	Mix10		M10.16	#N/A	TRUE
M2	Mix10		M10.17	#N/A	TRUE
M20	Mix10		M10.18	#N/A	TRUE
M23	Mix10		M10.19	#N/A	TRUE
M3	Mix10		M10.20	#N/A	TRUE
M4	Mix10		M10.21	#N/A	TRUE
M5	Mix10		M10.22	#N/A	TRUE
M6	Mix10		M10.23	#N/A	TRUE
M7	Mix10		M10.24	#N/A	TRUE
M8	Mix10		M10.25	#N/A	TRUE
M9	Mix10		M10.26	#N/A	TRUE
M25	Mix10		M10.27	#N/A	TRUE
M26	Mix10		M10.28	#N/A	TRUE
M27	Mix10		M10.29	#N/A	TRUE
M28	Mix10		M10.52	#N/A	FALSE
M29	Mix10		M10.30	#N/A	TRUE
M30	Mix10		M10.31	#N/A	TRUE
M31	Mix10		M10.32	#N/A	TRUE
M32	Mix10		M10.33	#N/A	TRUE
M33	Mix10		M10.34	#N/A	TRUE
M34	Mix10		M10.35	#N/A	TRUE
M35	Mix10		M10.36	#N/A	TRUE
M36	Mix10		M10.37	#N/A	TRUE
M37	Mix10		M10.38	#N/A	TRUE
M38	Mix10		M10.39	#N/A	TRUE
M39	Mix10		M10.40	#N/A	TRUE
M40	Mix10		M10.41	#N/A	TRUE
M41	Mix10		M10.42	#N/A	TRUE
M42	Mix10		M10.43	#N/A	TRUE
M43	Mix10		M10.44	#N/A	TRUE
M44	Mix10		M10.45	#N/A	TRUE
M45	Mix10		M10.46	#N/A	TRUE
M46	Mix10		M10.47	#N/A	TRUE
247			247	Rhodococcus	TRUE
265			265	Pseudorhodoferax	TRUE
283			283	Agreia	TRUE
341			341	Rhizobium	TRUE
369			369	Geodermatophilus	TRUE
384			384	Rhizobium	TRUE
415			415	Frigoribacterium	TRUE
436			436	Microbacterium	TRUE
84			84	Acidovorax	TRUE
126			126	Duganella	TRUE
131			131	Xanthomonas	TRUE
216			216	Pedobacter	TRUE
226			226	Sphingomonas	TRUE
230			230	Sphingomonas	TRUE
231			231	Sphingomonas	TRUE
242			242	Sphingomonas	TRUE
254			254	Frigoribacterium	TRUE
30			30	Sphingomonas	TRUE
304			304	Frondihabitans	TRUE
32			32	Sphingomonas	TRUE
343			343	Sphingomonas	TRUE
407			407	Sphingomonas	TRUE
210			210	Agreia	TRUE
261			261	Curtobacterium	TRUE
278			278	Rhodococcus	TRUE
291			291	Aeromicrobium	TRUE
294			294	Rathayibacter	TRUE
306			306	Rhizobium	TRUE
M23-261-291	Mix_minus		M10.19-261-291	#N/A	TRUE
M34-278	Mix_minus		M10.35-278	#N/A	TRUE
M37-210	Mix_minus		M10.38-210	#N/A	TRUE
M4-294-306	Mix_minus		M10.21-294-306	#N/A	TRUE
R11-278	Mix_minus		M10.48-278	#N/A	TRUE
comm		Remove_otherRepM34	comm	#N/A	FALSE
comm+Fr1		Remove_otherRepM34	comm+Fr1	#N/A	FALSE
181			181	#N/A	FALSE
190			190	#N/A	FALSE
229			229	#N/A	FALSE
239			239	#N/A	FALSE
27			27	#N/A	FALSE
288			288	Microbacterium	TRUE
302			302	#N/A	FALSE
355			355	#N/A	FALSE
36			36	#N/A	FALSE
373			373	#N/A	FALSE
387			387	#N/A	FALSE
409			409	#N/A	FALSE
418			418	#N/A	FALSE
422			422	#N/A	FALSE
43			43	#N/A	FALSE
439			439	#N/A	FALSE
444			444	#N/A	FALSE
445			445	#N/A	FALSE
45			45	#N/A	FALSE
56			56	#N/A	FALSE
6			6	#N/A	FALSE
70			70	Stenotrophomonas	TRUE
81			81	#N/A	FALSE
83			83	Pseudomonas	TRUE
139			139	Massilia	TRUE
14			14	#N/A	FALSE
158			158	#N/A	FALSE
166			166	#N/A	FALSE
20			20	Sphingomonas	TRUE
211			211	#N/A	FALSE
212			212	#N/A	FALSE
219			219	#N/A	FALSE
223			223	#N/A	FALSE
224			224	#N/A	FALSE
237			237	#N/A	FALSE
243			243	#N/A	FALSE
256			256	#N/A	FALSE
315			315	#N/A	FALSE
338			338	#N/A	FALSE
375			375	#N/A	FALSE
390			390	#N/A	FALSE
403			403	#N/A	FALSE
447			447	#N/A	FALSE
449			449	#N/A	FALSE
457			457	#N/A	FALSE
52			52	#N/A	FALSE
63			63	#N/A	FALSE
69			69	Arthrobacter	TRUE
NA			NA	#N/A	FALSE
234			234	Arthrobacter	TRUE
257			257	Sphingomonas	TRUE
296			296	Rathayibacter	TRUE
148			148	Xanthomonas	TRUE
17			17	Sphingomonas	TRUE
171			171	Plantibacter	TRUE
196			196	Exiguobacterium	TRUE
198			198	Sphingomonas	TRUE
220			220	Variovorax	TRUE
222			222	Agromyces	TRUE
443			443	Aurantimonas	TRUE
446			446	Marmoricola	TRUE
258			258	Rhodococcus	TRUE
7			7	Rhodococcus	TRUE
37			37	Sphingomonas	TRUE
456			456	Methylobacterium	TRUE
177			177	Burkholderia	TRUE
465			465	Methylobacterium	TRUE
49			49	Bacillus	TRUE
145			145	Arthrobacter	TRUE
155			155	Rhizobium	TRUE
73			73	Acidovorax	TRUE
98			98	Pseudomonas	TRUE
121			121	Methylobacterium	TRUE
130			130	Acinetobacter	TRUE
187			187	Exiguobacterium	TRUE
191			191	Acidovorax	TRUE
2			2	Novosphingobium	TRUE
434			434	Pseudomonas	TRUE
82			82	Flavobacterium	TRUE
293			293	#N/A	FALSE
408			408	Methylophilus	TRUE
93			93	Methylobacterium	TRUE
201			201	Chryseobacterium	TRUE
357			357	Sphingomonas	TRUE
58			58	Pseudomonas	TRUE
189			189	Dyadobacter	TRUE
285			285	Nocardioides	TRUE
289			289	Aeromicrobium	TRUE
363			363	Brevundimonas	TRUE
391			391	Rhizobium	TRUE
420			420	Devosia	TRUE
427			427	Aurantimonas	TRUE
68			68	Rhizobium	TRUE
86			86	Methylobacterium	TRUE
88			88	Methylobacterium	TRUE
90			90	Methylobacterium	TRUE
FA2			FA2	#N/A	FALSE
129			129	Pseudomonas	TRUE
141			141	Arthrobacter	TRUE
160			160	Acidovorax	TRUE
262			262	Rhizobium	TRUE
3			3	Sanguibacter	TRUE
324			324	Aureimonas	TRUE
334			334	Cellulomonas	TRUE
337			337	Arthrobacter	TRUE
344			344	Bosea	TRUE
351			351	Microbacterium	TRUE
44			44	Frigoribacterium	TRUE
53			53	Erwinia	TRUE
61			61	Duganella	TRUE
102			102	Methylobacterium	TRUE
122			122	Methylobacterium	TRUE
123			123	Methylobacterium	TRUE
125			125	Methylobacterium	TRUE
127			127	Pseudomonas	TRUE
13			13	Bacillus	TRUE
151			151	Microbacterium	TRUE
159			159	Microbacterium	TRUE
161			161	Microbacterium	TRUE
244			244	Agreia	TRUE
75			75	Bacillus	TRUE
76			76	Acidovorax	TRUE
78			78	Acidovorax	TRUE
8			8	Frigoribacterium	TRUE
99			99	Methylobacterium	TRUE
1			1	Plantibacter	TRUE
104			104	Methylobacterium	TRUE
106			106	Methylobacterium	TRUE
108			108	Methylobacterium	TRUE
112			112	Methylobacterium	TRUE
113			113	Methylobacterium	TRUE
117			117	Methylobacterium	TRUE
119			119	Methylobacterium	TRUE
164			164	Rathayibacter	TRUE
167			167	Rhizobium	TRUE
168			168	Brevundimonas	TRUE
172			172	Clavibacter	TRUE
180			180	Chryseobacterium	TRUE
185			185	Rathayibacter	TRUE
186			186	Frigoribacterium	TRUE
51			51	Serratia	TRUE
59			59	Pseudomonas	TRUE
64			64	Devosia	TRUE
170			170	Pedobacter	TRUE
263			263	Clavibacter	TRUE
264			264	Leifsonia	TRUE
281			281	#N/A	FALSE
307			307	Nocardioides	TRUE
311			311	Rhizobium	TRUE
313			313	Chryseobacterium	TRUE
314			314	Plantibacter	TRUE
320			320	Microbacterium	TRUE
336			336	Leifsonia	TRUE
339			339	Sphingomonas	TRUE
361			361	Methylobacterium	TRUE
386			386	Rhizobium	TRUE
394			394	Chryseobacterium	TRUE
100			100	Methylobacterium	TRUE
132			132	Pedobacter	TRUE
137			137	Arthrobacter	TRUE
248			248	Rathayibacter	TRUE
250			250	Pedobacter	TRUE
299			299	Rathayibacter	TRUE
321			321	Rhizobium	TRUE
325			325	Leifsonia	TRUE
354			354	Williamsia	TRUE
383			383	Rhizobium	TRUE
50			50	Serratia	TRUE
85			85	Methylobacterium	TRUE
87			87	Methylobacterium	TRUE
89			89	Methylobacterium	TRUE
92			92	Methylobacterium	TRUE
94			94	Methylobacterium	TRUE
22			22	Sphingomonas	TRUE
23			23	Sphingomonas	TRUE
24			24	Sphingomonas	TRUE
26			26	Sphingobium	TRUE
272			272	Aeromicrobium	TRUE
33			33	Sphingomonas	TRUE
335			335	Agreia	TRUE
34			34	Sphingomonas	TRUE
38			38	Sphingomonas	TRUE
380			380	Blastococcus	TRUE
399			399	Methylobacterium	TRUE
400			400	Acidovorax	TRUE
404			404	Chryseobacterium	TRUE
416			416	Methylophilus	TRUE
466			466	Methylobacterium	TRUE
469			469	Methylobacterium	TRUE
10			10	Sphingomonas	TRUE
11			11	Sphingomonas	TRUE
25			25	Sphingomonas	TRUE
267			267	Variovorax	TRUE
274			274	Pseudorhodoferax	TRUE
326			326	Deinococcus	TRUE
350			350	Aeromicrobium	TRUE
374			374	Nocardioides	TRUE
395			395	Cellulomonas	TRUE
396			396	Bradyrhizobium	TRUE
4			4	Sphingomonas	TRUE
401			401	Bradyrhizobium	TRUE
406			406	Bacillus	TRUE
414			414	Methylophilus	TRUE
454			454	Aurantimonas	TRUE
459			459	Methylophilus	TRUE
460			460	Aurantimonas	TRUE
9			9	Sphingomonas	TRUE
16			16	Sphingomonas	TRUE
182			182	Brevibacillus	TRUE
194			194	Pedobacter	TRUE
280			280	Brevundimonas	TRUE
29			29	Sphingomonas	TRUE
359			359	Flavobacterium	TRUE
41			41	Pedobacter	TRUE
42			42	Sphingomonas	TRUE
5			5	Sphingomonas	TRUE
62			62	Sphingomonas	TRUE
176			176	Pedobacter	TRUE
412			412	Sphingomonas	TRUE
142			142	#N/A	FALSE
144			144	#N/A	FALSE
429			429	#N/A	FALSE
57			57	#N/A	FALSE
156			156	#N/A	FALSE
165			165	#N/A	FALSE
192			192	#N/A	FALSE
356			356	#N/A	FALSE
424			424	#N/A	FALSE
428			428	#N/A	FALSE
228			228	#N/A	FALSE
246			246	#N/A	FALSE
157			157	#N/A	FALSE
448			448	#N/A	FALSE
203			203	Microbacterium	TRUE
M34-278-261	Mix_minus		M10.35-278-261	#N/A	TRUE
