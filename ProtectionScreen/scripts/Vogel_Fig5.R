# This script is to make the bak1bkk1 figure only containing information on strains that were tested with bak1bkk1 and also each experiment shown individually.
# 
# Part1: data selection
# 

# setup environment -------------------------------------------------------
library(tidyverse)
library(readxl)

library(scales)
library(ggpubr)
#Info on treatment for each well
MetaDataWell <- read.delim("data/MetaData/metaDataProtection.txt", header =T, stringsAsFactors = F)
# Luminescence raw data each well
LuminescenceData <- read.delim("data/CleanData/LuminescenceData_All_SinglePlants.txt", header =T, stringsAsFactors = F)
# Disease raw data each well
DiseaseData <- read.delim("data/CleanData/DiseaseData_All_SinglePlants.txt", header =T, stringsAsFactors = F)
colorsphylo <- readRDS("data/AtSPHEREStrain/colorsTreatments.RDS")

# MetaDataWell -> filter to keep only the wells we want to use in analysis and the treatments
ValWells <- MetaDataWell %>% 
  filter(!grepl("#N/A", Strain_ID.new)) %>% 
  filter(grepl("Val", Round)& Part=="I") %>% 
  filter(!(Action %in% c("REMOVE", "NOT_INFECTED", "INFECTED_THOUGH_NOT_WANTED")))
bbtreatments <- ValWells %>% select(Strain_ID.new) %>% unique() %>% pull(Strain_ID.new)
ValWells <- ValWells %>% filter(!(Matching =="Val2E.2ROI 1653")) # strong outlier not infected -> measurement problem


# For revision only keep the protective strains for Figure 5 and remove the other tested ones. 
# Protection Data overall of Supplementary Table 1 from the old submission
SuppTableOverview <- read_xlsx("//gram/biol_micro_vorholt/Group/Manuscripts/2020_Protection_screen/revision/SupplementaryTables_revision.xlsx", sheet = 1, skip = 3)
SuppTableOverview <- SuppTableOverview %>% rename(meanProt = `mean [a.u.]...5`, meanLumRed = `mean [a.u.]...9`)

# ProtInt <- SuppTableOverview %>% filter(meanProt >=50) %>% pull(Strain_ID)
Prot <-  SuppTableOverview %>% filter(meanProt >=75) %>% pull(Strain_ID)
bbTestedProt<- bbtreatments[bbtreatments %in% Prot]

# combination of ROund Plate and Well for filtering
RoundPlateWell <- paste(ValWells$Round, ValWells$Plate, ValWells$ROI, sep = "_")

# Select the Luminescence Data (note that we need to correct the metadata for the luminescence data)
LumDataVal <- LuminescenceData %>% 
  filter(grepl("Val", Round) & Part =="I") %>% 
  mutate(FilterCond = paste(Round, Plate, ROI, sep ="_")) %>%
  filter(FilterCond %in% RoundPlateWell) %>%
  filter(!(Action %in% c("only_disease"))) %>%
  filter(Strain_ID.new !="M3.3")

# stopifnot(length(ValWells$Round) == length(LumDataVal$Round)) # not the same because not for all luminescence data. 

# Check for each validation whether the same
# for(i in c("Val1", "Val2", "Val3", "Val4")){
#   if(length(LumDataVal$Round[LumDataVal$Round ==i])!= length(ValWells$Round[ValWells$Round ==i])){
#     warning(sprintf("not the same length for %s", i))
#   }
# }


# both Val1 and Val2 are a problem

unique(LumDataVal$Plate[LumDataVal$Round =="Val1"])
unique(LumDataVal$Plate[LumDataVal$Round =="Val2"])
# For Validation 1, we are missing the luminescence data for H.3 and M.3. For validation 2, we are missing the luminescence data for M.1. 
# For these the data apparently was not correctly saved when measuring IVIS. We cannot undo this.
# The treatments for these were:
#  Leaf58 (for H.3 in Val1); Leaf141 (For M.3 in Val1) and Leaf203 (for M.1 in Val2)


# write.table(LumDataVal,file ="data/CleanData/bak1bkk1Figure_LumDataVal_20210608.txt", sep = "\t", row.names=F)


# Do the figure only with the ones that are protective and with ax --------
LumDataValProt <- LumDataVal %>% filter(Strain_ID.new %in% c("Ax", Prot))
LumDataValProt <- LumDataValProt %>% select(Round, Plate, ROI, Strain_ID.new, Infection, Genotype, Total.Flux)
LumDataValProt$Genotype <-  factor(LumDataValProt$Genotype, levels = c("Col-0", "bak1bkk1"))

rm( MetaDataWell, ValWells, DiseaseData, LuminescenceData) # all not needed for this specific plot

# Analysis by strain for bak1bkk1 vs Col-0 --------------------------------

LumDataVal$Genotype <- factor(LumDataVal$Genotype, levels = c("Col-0", "bak1bkk1"))

library(broom)
# Within strain comparisons -----------------------------------------------
# Use three Welch's tests to check for difference:
# Pst_Col-0 vs CTL_Col-0 -> effect of infection in Col-0 wildtype
# Pst_bb vs CTL_bb -> effect of infection in bak1bkk1
# Pst_bb vs Pst_Col-0 -> effect of plant genotype on infection
# Chose this instead of a linear model for each strains with planned contrasts because not all of them are normally distributed. Welch's test doesn't care too much about the normality assumption and 
# is dealing with the heteroscedasticy observed for some strains
# Patrick suggested to not care about the potential plate effect because it's not a invalid assumption that it is random. 

  output <- data.frame("estimate" = numeric(),"estimate1"  = numeric(),"estimate2" = numeric(),  
                       "statistic" = numeric(),  "p.value" = numeric(), "parameter" = numeric(),  
                       "conf.low" = numeric(),   "conf.high"  = numeric(),
                        "method" = character()  ,   "alternative" = character(), "SE" = numeric(),
                       "contrast" = character(), "Strain" = character())
  output$alternative <- character()
  output$method <- character()
  output$contrast <- character()
  output$Strain <- character()

  
  for(i in unique(LumDataVal$Strain_ID.new)){
      subdata <- LumDataVal %>% filter(LumDataVal$Strain_ID.new ==i)
      Comp1List <- t.test(log10(subdata[subdata$Infection =="Pst" & subdata$Genotype=="Col-0", "Total.Flux"]),
                      log10(subdata[subdata$Infection =="CTL" & subdata$Genotype=="Col-0", "Total.Flux"]), 
                      alternative = "greater" )
      Comp1 <- broom::tidy(Comp1List)
      Comp1$contrast <- "Pst_Col-0 vs CTL_Col-0"
      Comp1$Strain <- i
      Comp1$SE <- Comp1List$stderr
      
      Comp2List <- t.test(log10(subdata[subdata$Infection =="Pst" & subdata$Genotype=="bak1bkk1", "Total.Flux"]),
                                  log10(subdata[subdata$Infection =="CTL" & subdata$Genotype=="bak1bkk1", "Total.Flux"]), alternative = "greater" )
      Comp2 <- broom::tidy(Comp2List)
      Comp2$contrast <- "Pst_bb vs CTL_bb"
      Comp2$Strain <- i
      Comp2$SE <- Comp2List$stderr
      
      Comp3List <- t.test(log10(subdata[subdata$Infection =="Pst" & subdata$Genotype=="bak1bkk1", "Total.Flux"]),
                                  log10(subdata[subdata$Infection =="Pst" & subdata$Genotype=="Col-0", "Total.Flux"]), alternative = "greater" )
      Comp3 <- broom::tidy(Comp3List)
      Comp3$contrast <- "Pst_bb vs Pst_Col-0"
      Comp3$Strain <- i
      Comp3$SE <- Comp3List$stderr
      
    output <- rbind(output, Comp1, Comp2, Comp3)
   rm(Comp1, Comp2, Comp3, Comp1List, Comp2List, Comp3List, subdata)
  
  }
  
  # multiple testing correction with Benjamini-Hochberg
  output$p.adj.holm <- p.adjust(output$p.value, method = "holm")
  output$p.adj.BH <- p.adjust(output$p.value, method = "BH")
  # write.table(output, "output/revision/20210616_bb/WelchtTestsSimpleCompVal_20210616.txt", sep ="\t", row.names = F)
  
  outputnice <- output %>% select(Strain_ID = Strain, estimate, SE, t = statistic, df = parameter, p.value, p.adj.holm, p.adj.BH, contrast)
 
  outputnice_wide <- outputnice %>% pivot_wider(names_from = contrast, values_from = estimate:p.adj.BH)
  outputnice_wide <- outputnice_wide %>% select(Strain_ID, ends_with("Pst_Col-0 vs CTL_Col-0"), ends_with("Pst_bb vs CTL_bb"), ends_with("Pst_bb vs Pst_Col-0"))

  # write.table(outputnice_wide, "output/revision/20210616_bb/WelchtTestsSimpleCompVal_Wide_SuppTable_20210616.txt", row.names = F, sep="\t")
  
  
# Test whether there is a difference in the response to the pathogen in the bak1/bkk1 plant background by fitting individual models for each strain in conjunction with axenic control
  # use a gls probably because it's not an unreasonable assumption that indeed the variance depends on the actual value (i.e. higher variance at higher values)
library(rstatix)
library(ggpubr)
sink(file="output/revision/Figure5/20210714_sink_lm_models_bb_vs_Ax.txt")
    my.model.plots <- list()
  glance_list <- list()
  tidy_list <- list()
  for(i in unique(LumDataVal$Strain_ID)[!(unique(LumDataVal$Strain_ID) %in% c("Ax", "C6"))]){
    print(i)
    ExpProt <- LumDataVal %>% filter(Strain_ID.new ==i) %>% pull(Round) %>% unique(.)
    subdata <- LumDataVal %>% filter(Strain_ID.new %in% c("Ax", i)) %>% filter(Round %in% ExpProt) %>% filter(Infection =="Pst")
    subdata$Strain_ID.new <- factor(subdata$Strain_ID.new)
    subdata$Strain_ID.new <- relevel(subdata$Strain_ID.new, ref="Ax")
    subdata$Genotype <- factor(subdata$Genotype)
    subdata$Genotype <- relevel(subdata$Genotype, ref="Col-0")
    subdata$log10TotalFlux <- log10(subdata$Total.Flux)
    if(length(ExpProt)>1){
      # when several experiments once include this information in the model
        fit1Round <- lm(log10(Total.Flux)~Strain_ID.new*Genotype+Round, data = subdata)
        par(mfrow=c(2,2))
        plot(fit1Round, main = paste0(i, "_withRound"))
        my.model.plots[[i]] <- recordPlot()
        print("normality assumption with Round")
       print(shapiro_test(residuals(fit1Round)))
        outliers<- subdata %>% group_by( Genotype, Strain_ID.new, Round) %>% identify_outliers(log10TotalFlux)%>% select(Genotype, Strain_ID.new, Round, Plate, ROI, Infection, is.outlier, is.extreme)
        print(outliers)
        # Homogeneity of variance assumption
        print("Homogeneity of variance assumption")
        print(subdata %>% levene_test(log10(Total.Flux)~Strain_ID.new*Genotype))
  
        glance_sub <- glance(fit1Round)
        glance_sub$model <- paste0(i, "_withRound")
       glance_list[[paste0(i, "_withRound")]] <- glance_sub

       tidysub <- tidy(fit1Round)
       tidysub$model <- paste0(i, "_withRound")
       
       tidy_list[[paste0(i, "_withRound")]] <- tidysub
       
    }else{
    # do it without the Round for the others:
      fit1 <- lm(log10(Total.Flux)~Strain_ID.new*Genotype, data = subdata)
      par(mfrow=c(2,2))
      plot(fit1,main = paste0(i, "_woRound"))
      my.model.plots[[paste0(i, "_woRound")]] <- recordPlot()
      print("normality assumption without Round")
      print(shapiro_test(residuals(fit1)))
      outliers<- subdata %>% group_by( Genotype, Strain_ID.new) %>% identify_outliers(log10TotalFlux) %>% select(Genotype, Strain_ID.new, Round, Plate, ROI, Infection, is.outlier, is.extreme)
    print(outliers)
      
      # Homogeneity of variance assumption
    print("Homogeneity of variance assumption")
      print(subdata %>% levene_test(log10(Total.Flux)~Strain_ID.new*Genotype))
      
      glance_sub <- glance(fit1)
      glance_sub$model <- paste0(i, "_woRound")
      
      glance_list[[paste0(i, "_woRound")]] <- glance_sub
      
      tidysub <- tidy(fit1)
      tidysub$model <- paste0(i, "_woRound")
      
      tidy_list[[paste0(i, "_woRound")]] <- tidysub
      }
  }
  sink()  
  library(data.table)
  # Afterwards combine them all
  tidy_All <-   rbindlist(tidy_list)
  glance_All <- rbindlist(glance_list)
  
  
  write.table(tidy_All, "output/revision/Figure5/bak1bkk1_individual_models_vs_Axenic_interaction_20210714.txt", sep ="\t", row.names=F)
  write.table(glance_All, "output/revision/Figure5/bak1bkk1_glance_individual_models_vs_Axenic_interaction_20210714.txt", sep ="\t", row.names=F)
  
  
  # print all the model plots to pdf
  
  # 
  # pdf("output/revision/20210616_bb/bak1bkk1_lm_model_plots_20210616.pdf" )
  # for(i in 1:length(my.model.plots)){
  #  print( my.model.plots[[i]])
  # }
  # dev.off()
  
  
  
  # For the ones where I don't think that the homogeneity assumptions are truly met:
  library(nlme)
  library(data.table)
  t_test_list <- list()
  sink(file = "output/revision/Figure5/20210714_sink_gls_models_2.txt")
  for(i in c("137", "145", "148", "15", "155", "167", "177", "203", "21", 
             "257", "262", "434", "51", "53", "58", "59", "68", "98", "Fr1", "337", "44", "129")){
    print(i)
    ExpProt <- LumDataVal %>% filter(Strain_ID.new ==i) %>% pull(Round) %>% unique(.)
    subdata <- LumDataVal %>% filter(Strain_ID.new %in% c("Ax", i)) %>% filter(Round %in% ExpProt) %>% filter(Infection =="Pst")
    subdata$Strain_ID.new <- factor(subdata$Strain_ID.new)
    subdata$Strain_ID.new <- relevel(subdata$Strain_ID.new, ref="Ax")
    subdata$Genotype <- factor(subdata$Genotype)
    subdata$Genotype <- relevel(subdata$Genotype, ref="Col-0")
    subdata$log10TotalFlux <- log10(subdata$Total.Flux)
    if(length(ExpProt)>1){
      # when several experiments once include this information in the model
      fit1Roundgls_1 <- gls(log10(Total.Flux)~Strain_ID.new*Genotype+Round, data = subdata)
      plot(fit1Roundgls_1, main = paste0(i, "_withRound_gls_noweight"))
 
       fit1Roundgls_2 <- gls(log10(Total.Flux)~Strain_ID.new*Genotype+Round, data = subdata, weights = varIdent(~1|Strain_ID.new))
      plot(fit1Roundgls_2, main = paste0(i, "_withRound_gls_varIdnetStrain"))
      
      fit1Roundgls_3 <- gls(log10(Total.Flux)~Strain_ID.new*Genotype+Round, data = subdata, weights = varPower())
      plot(fit1Roundgls_3, main = paste0(i, "_withRound_gls_varIdnetStrain"))
      
      AIC(fit1Roundgls_1, fit1Roundgls_2, fit1Roundgls_3)
      BIC(fit1Roundgls_1, fit1Roundgls_2, fit1Roundgls_3)

      sum_gls_1 <- summary(fit1Roundgls_1)
      t_test1 <- as.data.frame(sum_gls_1$tTable)
      t_test1$AIC <- sum_gls_1$AIC
      t_test1$model <- paste0(i, "_GLS_withRound_novar")
      t_test1 <- setDT(t_test1, keep.rownames = "coef")[]
      print(paste0(i, "_GLS_withRound_novar"))
      print(sum_gls_1)
      
      t_test_list[[paste0(i, "_GLS_withRound_novar")]] <- t_test1
      
      sum_gls_3 <- summary(fit1Roundgls_3)
      t_test3 <- as.data.frame(sum_gls_3$tTable)
      t_test3$AIC <- sum_gls_3$AIC
      t_test3$model <- paste0(i, "_GLS_withRound_varPower")
      t_test3 <- setDT(t_test3, keep.rownames = "coef")[]
      print(paste0(i, "_GLS_withRound_varPower"))
      print(sum_gls_3)
      t_test_list[[paste0(i, "_GLS_withRound_varPower")]] <- t_test3
      rm(t_test1, t_test3)
    } else{
      # when one experiments once include this information in the model
      fit1woRoundgls_1 <- gls(log10(Total.Flux)~Strain_ID.new*Genotype, data = subdata)
      plot(fit1woRoundgls_1, main = paste0(i, "_woRound_gls_noweight"))
      # my.model.plots.gls.woRound[[i]] <- recordPlot()
      
      fit1woRoundgls_2 <- gls(log10(Total.Flux)~Strain_ID.new*Genotype, data = subdata, weights = varIdent(~1|Strain_ID.new))
      plot(fit1woRoundgls_2, main = paste0(i, "_woRound_gls_varIdnetStrain"))
      
      fit1woRoundgls_3 <- gls(log10(Total.Flux)~Strain_ID.new*Genotype, data = subdata, weights = varPower())
      plot(fit1woRoundgls_3, main = paste0(i, "_woRound_gls_varIdnetStrain"))
      
      print(AIC(fit1woRoundgls_1, fit1woRoundgls_2, fit1woRoundgls_3))
      # BIC(fit1woRoundgls_1, fit1woRoundgls_2, fit1woRoundgls_3)
      
      
      sum_gls_1 <- summary(fit1woRoundgls_1)
      t_test1 <- as.data.frame(sum_gls_1$tTable)
      t_test1$AIC <- sum_gls_1$AIC
      t_test1$model <- paste0(i, "_GLS_woRound_novar")
      t_test1 <- setDT(t_test1, keep.rownames = "coef")[]
      summary(fit1woRoundgls_1)
      print(paste0(i, "_GLS_woRound_novar"))
      print(sum_gls_1)
      
      sum_gls_3 <- summary(fit1woRoundgls_3)
      t_test3 <- as.data.frame(sum_gls_3$tTable)
      t_test3$AIC <- sum_gls_3$AIC
      t_test3$model <- paste0(i, "_GLS_woRound_varPower")
      t_test3 <- setDT(t_test3, keep.rownames = "coef")[]
      summary(fit1woRoundgls_3)
      print(paste0(i, "_GLS_woRound_varPower"))
      print(sum_gls_3)
      
      t_test_list[[paste0(i, "_GLS_woRound_novar")]] <- t_test1
      t_test_list[[paste0(i, "_GLS_woRound_varPower")]] <- t_test3
      
      rm(t_test1, t_test3)
    }
  }
  sink()

  t_test_lists_all <- rbindlist(t_test_list)
  write.table(t_test_lists_all, file = "output/revision/Figure5/20210714_gls_models_ttables.txt", sep="\t", row.names=F)
   
  
  
  # Get the overall table I want to get for the graph
  # Will just always use the experiment if it is available
  table(LumDataVal$Strain_ID.new, LumDataVal$Round)
  
  #For the supplementary table, I want to add the interaction term only
  SuppGLS <- t_test_lists_all %>% filter(grepl(":Genotypebak1bkk1", coef)) %>% filter(grepl("varPower", model)) %>% 
    filter(!grepl("Fr1", model)) %>% filter(!grepl("44", model))
  # I had checked and for all except for Fr1 and 44 the model with varPower better. For Fr1 and 44 can use the linear model. Should be fine.
  SuppGLS <- SuppGLS %>% mutate(Strain_ID.new = model) %>% separate(Strain_ID.new, into = c("Strain_ID.new", "modeltype"))
  SuppGLS
  # write.table(SuppGLS, "output/revision/20210616_bb/interactionterm_GLS_20210616.txt", sep="\t", row.names=F)
  
  #interaction for the others
  SuppLm_Interaction <- tidy_All %>% filter(grepl(":Genotypebak1bkk1", term)) %>% mutate(Strain_ID.new = model) %>% separate(Strain_ID.new, into = c("Strain_ID.new", "modeltype")) %>%
                        select(Strain_ID.new, estimate, std.error, t = statistic, p.value, modeltype, model)
  

  SuppGLS_Interaction <- SuppGLS %>% select(Strain_ID.new, estimate = Value, std.error = Std.Error, t=`t-value`, p.value = `p-value`, modeltype, model)
  
  StrainsGLS <- unique(SuppGLS_Interaction$Strain_ID.new)
  SuppLm_Interactionsub <- SuppLm_Interaction %>% filter(!(Strain_ID.new %in% StrainsGLS))
  
  SuppLm_Interactionsub$Model <- "LM"
  SuppGLS_Interaction$Model <- "GLS"
  
  InteractionStats <- rbind(SuppLm_Interactionsub, SuppGLS_Interaction)
  
  
  
  
  
  #Combine the Interaction stats and the outputnice in one single graph -> then export to Excel and modify there for the end
  InteractionStats$p.adj.BH <-p.adjust(InteractionStats$p.value, method = "BH")

   write.table(InteractionStats, "output/revision/Figure5/interactionStatisticsbb_20210714.txt", sep ="\t", row.names=F)
   
  
  InteractionsforSupp <- InteractionStats %>% mutate(df=NA)%>% select(Strain_ID = Strain_ID.new, estimate, SE = std.error, t,df, p.value, p.adj.BH) %>% mutate(p.adj.holm = 1)
  InteractionsforSupp$contrast <- "Strain_ID:Genotypebak1bkk1"
  # Now put together the supplementary table as before:
  
  CombinedStats <- bind_rows(outputnice, InteractionsforSupp)
  
  CombinedStats_wide <- CombinedStats %>% pivot_wider(names_from = contrast, values_from = estimate:p.adj.BH)
  CombinedStats_wide <- CombinedStats_wide %>% select(Strain_ID, ends_with("Pst_Col-0 vs CTL_Col-0"), ends_with("Pst_bb vs CTL_bb"), ends_with("Pst_bb vs Pst_Col-0"), ends_with("Strain_ID:Genotypebak1bkk1"))
  CombinedStats_wide <- CombinedStats_wide %>% select(-"p.adj.holm_Strain_ID:Genotypebak1bkk1")
  write.table(CombinedStats_wide, "output/revision/Figure5/Bak1bkk1_supplementary_Table_withInteraction_20210714.txt", sep ="\t", row.names=F)
  

# Grouping of strains -----------------------------------------------------

# based on 
# 1) difference between Pst bb vs Col-0? -> get no difference (group1) and difference (group2)


# significantly higher Pstbb vs Col-0
Strains_Group_not_aff_fdr <- CombinedStats_wide %>% filter(`p.adj.BH_Pst_bb vs Pst_Col-0` >0.05 | `estimate_Pst_bb vs Pst_Col-0`<0) %>% pull(Strain_ID)
Strains_Group2_aff_fdr  <- CombinedStats_wide %>% filter(`p.adj.BH_Pst_bb vs Pst_Col-0` <0.05 & `estimate_Pst_bb vs Pst_Col-0`>0) %>% pull(Strain_ID)


#Grouping by whether significantly higher Pstbb vs Col-0
CombinedStats_wide$Group_alt <- NA
CombinedStats_wide$Group_alt[CombinedStats_wide$Strain_ID %in% Strains_Group_not_aff_fdr] <- "Group1_alt"
CombinedStats_wide$Group_alt[CombinedStats_wide$Strain_ID %in% Strains_Group2_aff_fdr] <- "Group2_alt"


# Sort the strains by mean luminescence (in Col-0 Pst) and by Group.
LumDataValColPstSum <- LumDataVal %>%
  group_by(Strain_ID.new, Infection, Genotype) %>%
  summarise(n = n(), meanlog10Flux = mean(log10(Total.Flux)), medianlog10Flux = median(log10(Total.Flux)), medianFlux = median(Total.Flux))


LumDataValColPstSum_StrainOrder <- LumDataValColPstSum  %>% 
  filter(Genotype=="Col-0" & Infection =="Pst") %>% arrange(., medianFlux) 
StrainOrderWithoutGroup <- LumDataValColPstSum_StrainOrder$Strain_ID.new

# put together the strainOrder
StrainOrderWithoutGroup <- as.character(StrainOrderWithoutGroup)

# Plot with the correct comparisons ---------------------------------------
LumDataVal$Strain_ID.new <- factor(LumDataVal$Strain_ID.new, levels = StrainOrderWithoutGroup)
# Add grouping information to LumDataValProt

LumDataVal$Group_alt <- ""
LumDataVal$Group_alt[LumDataVal$Strain_ID.new %in% Strains_Group_not_aff_fdr] <- "Group1_alt"
LumDataVal$Group_alt[LumDataVal$Strain_ID.new %in% Strains_Group2_aff_fdr] <- "Group2_alt"

# for coloring the labels
StrainColors <- colorsphylo[StrainOrderWithoutGroup]
ColorLabelsdf <- LumDataVal %>% select(Strain_ID.new, Group_alt) %>% unique()
ColorLabelsdf <- ColorLabelsdf[match( ColorLabelsdf$Strain_ID.new,names(StrainColors)), ]
ColorLabelsdf$color <- StrainColors[as.character(ColorLabelsdf$Strain_ID.new)]
# install.packages("ggtext")
library(ggtext)

library(glue)
ColorLabelsdf <- ColorLabelsdf %>% mutate(NewName = glue("<span style='color:{color}'>{Strain_ID.new}</span>"))
ColorLabelsdf$Strain_ID.new


# LumDataValProt <- LumDataValProt %>% select(-NewName)
LumDataVal <- left_join(LumDataVal, ColorLabelsdf)

# to get the correct order -> sort the ColorLabelsdf first by levels of Strain_ID.new and then make a vector of the order of NewName 
ColorLabelsdf <- ColorLabelsdf[match(levels(ColorLabelsdf$Strain_ID.new), ColorLabelsdf$Strain_ID.new),]
NewNameOrder <- ColorLabelsdf$NewName

# 
# stat.test <- LumDataValProt %>% select(-NewName) %>% left_join(ColorLabelsdf, by = c("Strain_ID.new", "Group")) %>% mutate(log10TotalFlux = log10(Total.Flux)) %>% 
#   group_by(NewName, Group) %>% t_test(log10TotalFlux~Genotype, alternative = "less") %>% adjust_pvalue() %>%
#   mutate(y.position = 10^8) %>% filter(p.adj < 0.05) 

LumDataVal$NewName <- factor(LumDataVal$NewName, levels = NewNameOrder)
LumDataValPlot <- LumDataVal[LumDataVal$Infection =="Pst",]

  
statsForPlot.fdr <- CombinedStats_wide %>% 
  select(Strain_ID, ends_with("Pst_bb vs Pst_Col-0"),  Group_alt)


statsForPlot.fdr <- left_join(statsForPlot.fdr, ColorLabelsdf, by = c("Strain_ID" = "Strain_ID.new", "Group_alt")) %>%
  select(Strain_ID, NewName,  Group_alt, `p.adj.BH_Pst_bb vs Pst_Col-0`) %>% unique()




ggplot(data = LumDataVal[LumDataVal$Strain_ID.new %in% c(Prot, "Ax")& LumDataVal$Infection =="Pst",])+
  geom_jitter(alpha = 0.8, size = 0.5, position = position_jitterdodge(dodge.width = 0.75, jitter.width=0.3),aes(group = Genotype, x=NewName, y = Total.Flux, color=Genotype, shape = Round))+
  geom_boxplot(outlier.shape = NA,  alpha = 0.4, aes(x=NewName, y = Total.Flux, fill=Genotype))+
  theme(axis.text.y = element_text(angle=90,vjust=0))+
  theme(axis.text.x = element_text(angle=90,vjust=0.5))+
  scale_color_manual(values = setNames(c("grey14", "black"), nm = c("Col-0", "bak1bkk1")))+
  scale_y_log10(limits = c(10^4, 5*10^8 ), breaks = trans_breaks("log10", function(x) 10^x),
                labels = trans_format("log10", math_format(10^.x))) +
  scale_fill_manual(values = setNames(c("white", "blue"), nm = c("Col-0", "bak1bkk1")))+
  scale_shape_manual(values = c(21,22,23,24))+
  theme(  panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          text = element_text(size = 10),
          plot.title = element_text(size = 10),
          panel.background = element_rect(fill="grey97"))+
  theme(strip.background =element_blank())+
  theme(strip.text.x = element_text(angle=90))+
  theme(axis.text.x = element_markdown())+
  theme(legend.position = "bottom")+
  ylab("Luminescence [p/s]")+
  xlab("")+  
  geom_text(data = statsForPlot.fdr[statsForPlot.fdr$`p.adj.BH_Pst_bb vs Pst_Col-0`<0.05 & statsForPlot.fdr$Strain_ID %in% c(Prot, "Ax"),],  aes(x = NewName, y = 3*10^8,label = round(`p.adj.BH_Pst_bb vs Pst_Col-0`, digits = 3)), angle = 90, size = 3)+
  geom_text(data = statsForPlot.fdr[statsForPlot.fdr$`p.adj.BH_Pst_bb vs Pst_Col-0`>0.05& statsForPlot.fdr$Strain_ID %in% c(Prot, "Ax"),],  aes(x = NewName, y = 3*10^8,label = "ns"), angle = 90, size = 3)+
  facet_grid(.~Group_alt, scales = "free_x", space = "free_x")

ggsave(filename = "output/revision/Figure5/StartingPoint_R2_2.pdf", useDingbats=F, width =7.5, height =  4)


notProt <- as.character(unique(LumDataVal$Strain_ID.new)[!unique(LumDataVal$Strain_ID.new) %in% c(Prot, "M3.3")])

ggplot(data = LumDataVal[LumDataVal$Strain_ID.new %in% c(notProt, "Fr1") & LumDataVal$Infection =="Pst",])+
  geom_jitter(alpha = 0.8, size = 0.5, position = position_jitterdodge(dodge.width = 0.75, jitter.width=0.3),aes(group = Genotype, x=NewName, y = Total.Flux, color=Genotype, shape = Round))+
  geom_boxplot(outlier.shape = NA,  alpha = 0.4, aes(x=NewName, y = Total.Flux,fill=Genotype))+
  theme(axis.text.y = element_text(angle=90,vjust=0))+
  theme(axis.text.x = element_text(angle=90,vjust=0.5))+
  scale_color_manual(values = setNames(c("grey14", "black"), nm = c("Col-0", "bak1bkk1")))+
  scale_y_log10(limits = c(10^4, 5*10^8 ), breaks = trans_breaks("log10", function(x) 10^x),
                labels = trans_format("log10", math_format(10^.x))) +
  scale_fill_manual(values = setNames(c("white", "blue"), nm = c("Col-0", "bak1bkk1")))+
  scale_shape_manual(values = c(21,22,23,24))+
  theme(  panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          text = element_text(size = 10),
          plot.title = element_text(size = 10),
          panel.background = element_rect(fill="grey97"))+
  theme(strip.background =element_blank())+
  theme(strip.text.x = element_text(angle=90))+
  theme(axis.text.x = element_markdown())+
  theme(legend.position = "bottom")+
  ylab("Luminescence [p/s]")+
  xlab("")+  
  geom_text(data = statsForPlot.fdr[statsForPlot.fdr$`p.adj.BH_Pst_bb vs Pst_Col-0`<0.05 & statsForPlot.fdr$Strain_ID %in% c(notProt, "Fr1"),],  aes(x = NewName, y = 3*10^8,label = round(`p.adj.BH_Pst_bb vs Pst_Col-0`, digits = 3)), angle = 90, size = 3)+
  geom_text(data = statsForPlot.fdr[statsForPlot.fdr$`p.adj.BH_Pst_bb vs Pst_Col-0`>0.05& statsForPlot.fdr$Strain_ID %in% c(notProt, "Fr1"),],  aes(x = NewName, y = 3*10^8,label = "ns"), angle = 90, size = 3)+
  facet_grid(.~Group_alt, scales = "free_x", space = "free_x")

ggsave(filename = "output/revision/Figure5/SupplementaryFigure_startingPoint.pdf", useDingbats=F, width =7.5, height =  4)


statsForPlot.holm <- CombinedStats_wide %>% 
  select(Strain_ID, ends_with("Pst_bb vs Pst_Col-0"),  Group_alt) %>% left_join(ColorLabelsdf, by = c("Strain_ID" = "Strain_ID.new", "Group_alt")) %>%
  select(Strain_ID, NewName,  Group_alt, `p.adj.holm_Pst_bb vs Pst_Col-0`) %>% unique()
GroupAffected_Holm <- statsForPlot.holm %>% filter(`p.adj.holm_Pst_bb vs Pst_Col-0` <0.05) %>% pull(Strain_ID) 
statsForPlot.holm$Group <- ifelse(statsForPlot.holm$Strain_ID %in% GroupAffected_Holm, "Affected", "NotAffected")
statsForPlot.holm$Group <- factor(statsForPlot.holm$Group, levels = c("NotAffected", "Affected"))
LumDataVal$Group <- ifelse(LumDataVal$Strain_ID.new %in% GroupAffected_Holm, "Affected", "NotAffected")

statsForPlot.holm$Strain_ID <- factor(statsForPlot.holm$Strain_ID, levels = StrainOrderWithoutGroup)

LumDataVal$Group <- factor(LumDataVal$Group, levels = c("NotAffected", "Affected"))

ggplot(data = LumDataVal[LumDataVal$Strain_ID.new %in% c(Prot, "Ax")& LumDataVal$Infection =="Pst",])+
  geom_jitter(alpha = 0.8, size = 0.5, position = position_jitterdodge(dodge.width = 0.75, jitter.width=0.3),aes(group = Genotype, x=NewName, y = Total.Flux, color=Genotype, shape = Round))+
  geom_boxplot(outlier.shape = NA,  alpha = 0.4, aes(x=NewName, y = Total.Flux,fill=Genotype))+
  theme(axis.text.y = element_text(angle=90,vjust=0))+
  theme(axis.text.x = element_text(angle=90,vjust=0.5))+
  scale_color_manual(values = setNames(c("grey14", "black"), nm = c("Col-0", "bak1bkk1")))+
  scale_y_log10(limits = c(10^4, 5*10^8 ), breaks = trans_breaks("log10", function(x) 10^x),
                labels = trans_format("log10", math_format(10^.x))) +
  scale_fill_manual(values = setNames(c("white", "blue"), nm = c("Col-0", "bak1bkk1")))+
  scale_shape_manual(values = c(21,22,23,24))+
  theme(  panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          text = element_text(size = 10),
          plot.title = element_text(size = 10),
          panel.background = element_rect(fill="grey97"))+
  theme(strip.background =element_blank())+
  theme(strip.text.x = element_text(angle=90))+
  theme(axis.text.x = element_markdown())+
  theme(legend.position = "bottom")+
  ylab("Luminescence [p/s]")+
  xlab("")+  
  geom_text(data = statsForPlot.holm[statsForPlot.holm$`p.adj.holm_Pst_bb vs Pst_Col-0`<0.05 & statsForPlot.holm$Strain_ID %in% c(Prot, "Ax"),],  aes(x = NewName, y = 3*10^8,label = round(`p.adj.holm_Pst_bb vs Pst_Col-0`, digits = 3)), angle = 90, size = 3)+
  geom_text(data = statsForPlot.holm[statsForPlot.holm$`p.adj.holm_Pst_bb vs Pst_Col-0`>0.05& statsForPlot.holm$Strain_ID %in% c(Prot, "Ax"),],  aes(x = NewName, y = 3*10^8,label = "ns"), angle = 90, size = 3)+
  facet_grid(.~Group, scales = "free_x", space = "free_x")

ggsave(filename = "output/revision/Figure5/StartingPoint_R2_2_holm.pdf", useDingbats=F, width =7.5, height =  4)


notProt <- as.character(unique(LumDataVal$Strain_ID.new)[!unique(LumDataVal$Strain_ID.new) %in% c(Prot, "M3.3")])

ggplot(data = LumDataVal[LumDataVal$Strain_ID.new %in% c(notProt, "Fr1")& LumDataVal$Infection =="Pst",])+
  geom_jitter(alpha = 0.8, size = 0.5, position = position_jitterdodge(dodge.width = 0.75, jitter.width=0.3),aes(group = Genotype, x=NewName, y = Total.Flux, color=Genotype, shape = Round))+
  geom_boxplot(outlier.shape = NA,  alpha = 0.4, aes(x=NewName, y = Total.Flux,fill=Genotype))+
  theme(axis.text.y = element_text(angle=90,vjust=0))+
  theme(axis.text.x = element_text(angle=90,vjust=0.5))+
  scale_color_manual(values = setNames(c("grey14", "black"), nm = c("Col-0", "bak1bkk1")))+
  scale_y_log10(limits = c(10^4, 5*10^8 ), breaks = trans_breaks("log10", function(x) 10^x),
                labels = trans_format("log10", math_format(10^.x))) +
  scale_fill_manual(values = setNames(c("white", "blue"), nm = c("Col-0", "bak1bkk1")))+
  scale_shape_manual(values = c(21,22,23,24))+
  theme(  panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          text = element_text(size = 10),
          plot.title = element_text(size = 10),
          panel.background = element_rect(fill="grey97"))+
  theme(strip.background =element_blank())+
  theme(strip.text.x = element_text(angle=90))+
  theme(axis.text.x = element_markdown())+
  theme(legend.position = "bottom")+
  ylab("Luminescence [p/s]")+
  xlab("")+  
  geom_text(data = statsForPlot.holm[statsForPlot.holm$`p.adj.holm_Pst_bb vs Pst_Col-0`<0.05 & statsForPlot.holm$Strain_ID %in% c(notProt, "Fr1"),],  aes(x = NewName, y = 3*10^8,label = round(`p.adj.holm_Pst_bb vs Pst_Col-0`, digits = 3)), angle = 90, size = 3)+
  geom_text(data = statsForPlot.holm[statsForPlot.holm$`p.adj.holm_Pst_bb vs Pst_Col-0`>0.05& statsForPlot.holm$Strain_ID %in% c(notProt, "Fr1"),],  aes(x = NewName, y = 3*10^8,label = "ns"), angle = 90, size = 3)+
  facet_grid(.~Group, scales = "free_x", space = "free_x")

ggsave(filename = "output/revision/Figure5/SupplementaryFigure_startingPoint_holm.pdf", useDingbats=F, width =7.5, height =  4)
















sink("output/revision/Figure5/sessionInfo_bb_stats_20210714_atlab.txt")
sessionInfo()
sink()


# Prepare Raw data for Figure 5 -------------------------------------------

# write to multiple sheet with openxlsx

# 5
Data_Fig5 <- LumDataVal %>% filter(Strain_ID.new %in% c(Prot, "Ax")& LumDataVal$Infection =="Pst") %>% select(Round, Plate, Strain_ID = Strain_ID.new, Infection, Genotype, Total.Flux, Group = Group_alt, color)
# 
# Data_Fig5_Stats_bbvsCol <- statsForPlot.fdr %>% select(Strain_ID, Group = Group_alt, p.adj_Pst_bbvsCol_greater = `fdr_Pst_bb vs Pst_Col-0`)
# Data_Fig5_Stats_bbvsCol$Group[Data_Fig5_Stats_bbvsCol$Group =="Group1_alt"] <- "not_affected_bb"
# Data_Fig5_Stats_bbvsCol$Group[Data_Fig5_Stats_bbvsCol$Group =="Group2_alt"] <- "affected_bb"
Data_Fig5$Group <- as.character(Data_Fig5$Group)

Data_Fig5$Group[Data_Fig5$Group =="Group1_alt"] = "Not_affected"
Data_Fig5$Group[Data_Fig5$Group =="Group2_alt"] = "Affected"


# Write the table to excel ------------------------------------------------
library(openxlsx)

write.xlsx(list("Fig5" = Data_Fig5), file = "output/revision/Figure5/SourceData_Figure5_20210714.xlsx")



# Source Data Supplementary Figure
Data_SuppFig <- LumDataVal %>% filter(Strain_ID.new %in% c(notProt, "Fr1")& LumDataVal$Infection =="Pst") %>% select(Round, Plate, Strain_ID = Strain_ID.new, Infection, Genotype, Total.Flux, Group = Group_alt, color)
# 
# Data_Fig5_Stats_bbvsCol <- statsForPlot.fdr %>% select(Strain_ID, Group = Group_alt, p.adj_Pst_bbvsCol_greater = `fdr_Pst_bb vs Pst_Col-0`)
# Data_Fig5_Stats_bbvsCol$Group[Data_Fig5_Stats_bbvsCol$Group =="Group1_alt"] <- "not_affected_bb"
# Data_Fig5_Stats_bbvsCol$Group[Data_Fig5_Stats_bbvsCol$Group =="Group2_alt"] <- "affected_bb"
Data_SuppFig$Group <- as.character(Data_SuppFig$Group)

Data_SuppFig$Group[Data_SuppFig$Group =="Group1_alt"] = "Not_affected"
Data_SuppFig$Group[Data_SuppFig$Group =="Group2_alt"] = "Affected"


write.xlsx(list("SFxx" = Data_Fig5), file = "output/revision/Figure5/SourceData_SupplementaryFigure_20210714.xlsx")
