# Script to make figure of Combis of strains:
# BCE of best strain vs mic

# First read in the mixes
library(readxl)
library(tidyverse)
library(ggplot2)
library(RColorBrewer)
library(ggpubr)
strainsmixes = read_xlsx("Data/MetaData/Combi3_4_Mixes_new.xlsx")

strainsmixeslong = strainsmixes %>%
  gather( Mix2, Strains , Strain_1:Strain11 )

# Then read and add the mean BCE -> use the values that were also used for Supp Figure 2:

SummaryBCE_ByStrain = readRDS("data/AugmentedData/SummaryBCE_ByStrain_20210630.RDS")
SummaryBCE_ByStrainCol0 = SummaryBCE_ByStrain %>% 
  filter(Genotype =="Col-0") %>%
  select(Strain_ID, Genotype, Infection, AtLSPHERE, BCE.13dpi.Strain, BCE.13dpi.wo0.Strain)

strainsmixeslong$Strains = as.character(strainsmixeslong$Strains)
strainsmixeslong = strainsmixeslong %>%
  dplyr::left_join(., SummaryBCE_ByStrainCol0, by= c("Strains" = "Strain_ID"))


# Now keep for each one the top strain with the highest BCE
highestpermix = strainsmixeslong %>%
  group_by(Mix) %>%
  top_n(1, BCE.13dpi.Strain)

  
# Now get the data for all mixes
SummaryBCE_ByExp <- readRDS("data/AugmentedData/SummaryBCE_ByExp_20210630.RDS")

# subset so only mixes data is kept. Note that I amended the * for supptable.
SummaryBCE_ByExpMix <- SummaryBCE_ByExp%>%
  dplyr::filter(Genotype =="Col-0") %>%
  dplyr::filter(Round %in% c("Combi3", "Combi4")) %>%
  dplyr::filter(grepl("R|M", Strain_ID))

SumDisDataByPartCombined <- highestpermix %>%
  select(Mix, Round, Strains, BCE.13dpi.Strain) %>%
  dplyr::left_join(., SummaryBCE_ByExpMix, by = c("Mix" = "Strain_ID", "Round")) %>%
  rename(meanBCE.bestStrain = BCE.13dpi.Strain, bestStrain = Strains)
 
write.table(SumDisDataByPartCombined,"data/AugmentedData/SupplementaryTableMixes_BCEvssingle_20210709.txt", sep="\t", row.names=F)


# Read in the modified file
Datamixes = read.delim("data/AugmentedData/SupplementaryTableMixes_BCEvssingle_20210709.txt", header=T)
Datamixes$BCE = Datamixes$BCE.13dpi.Exp
Datamixes$Group <- ifelse(grepl("-", Datamixes$Mix), "reduced", "All")

colorsSpez <- setNames(c("grey", brewer.pal(6, "Accent")[c(1:3,5:6)]), nm = c("black", "M10.19", "M10.21","M10.48" , "M10.38","M10.35" ))


theme_own = function(base_size = 11, base_family = "",base_line_size = base_size/22, 
                     base_rect_size = base_size/22)
{
  theme_grey(base_size = base_size, base_family = base_family, 
             base_line_size = base_line_size, base_rect_size = base_rect_size) %+replace% 
    theme(panel.background = element_rect(fill = "white", 
                                          colour = NA), panel.border = element_rect(fill = NA, 
                                                                                    colour = "grey20"), panel.grid = element_line(colour = "grey92"), 
          panel.grid.minor = element_blank(), panel.grid.major.x = element_blank(),
          strip.background = element_rect(fill = "grey85", 
                                          colour = "grey20"), legend.key = element_rect(fill = "white", 
                                                                                        colour = NA), complete = TRUE,
          axis.text.x = element_text(angle = 90, hjust = 1))
}


# When we only take the Combi3 data and not the one with the extra strains:
library(ggrepel)
DatamixesVerygood <- Datamixes[Datamixes$Round=="Combi3" & Datamixes$BCE.13dpi.Exp>75,]
DatamixesVerygood$Label <- DatamixesVerygood$Strain_ID.new

Figure4a <- ggplot(Datamixes[Datamixes$Round =="Combi3" & !(Datamixes$Strain_ID.new %in% c("M10.43", "M10.51")),], aes(x = meanBCE.bestStrain, y = BCE)) +
  theme_own(base_size = 11)+
  geom_abline(color = "lightgrey")+
  geom_point(aes(shape = Group),alpha = 1, size= 2)+
  geom_text_repel(aes(label=Label), size=2, data=DatamixesVerygood)+
  theme(panel.background = element_blank(), panel.grid=element_line(colour="grey92"),
        panel.grid.minor=element_blank(), panel.grid.major.x = element_blank(), panel.border=element_blank())+
  theme(legend.background = element_rect(fill="white", colour=NA), legend.key=element_rect(fill = "white", colour = NA),
        legend.position = "bottom")+
  theme(axis.text.x=element_text(angle=0, hjust=0.5, vjust=0.5))+
  scale_x_continuous("best single strain protection score [a.u.]", limits = c(0, 100))+
  scale_y_continuous("SynCom protection score [a.u.]", limits = c(0,100))+
   theme(legend.position = "none")+
  coord_fixed()

saveRDS(Figure4a, "output/M10_Figure4/Figure4a.RDS")
ggsave(filename="FiguresManuscript/M10_M34/Combi3_mixes_vs_best_single_strain_20210709.pdf", useDingbats=F, height=6, width=8, scale=0.6)


unique(Datamixes$Strain_ID.new[Datamixes$Round=="Combi4"])
# in Combi4 sind M10.35, M10.21, M10.22, M10.8, M10.19, M10.48, M10.43, M10.50, M10.38

# For M10.38 -> mix37, not the most abundant strain removed. based on color from plating i assume that either 222 or 314 were most abundant (or 446, 264 could also be possible)
# -> therefore pbably no stark drop with removal of this strain.
# -> maybe remove here.


# Get the BCE from Combi4 for the strains removed from the mixes. 
# Need to check whether the individual BCE of one strain that was removed in the Combi4 experiment was los
SummaryBCE_ByExp %>% filter(Strain_ID %in% c("278", "261", "291", "210"))

# The BCE of Leaf278 in Combi4 is higher than the overall BCE of this strain. Thus for Figure b exchange this
Datamixes$meanBCE.bestStrain[Datamixes$bestStrain == "278" & Datamixes$Round =="Combi4"] <- SummaryBCE_ByExp$BCE.13dpi.Exp[SummaryBCE_ByExp$Strain_ID =="278" & SummaryBCE_ByExp$Round=="Combi4"]
Datamixes$meanBCE.bestStrain[Datamixes$Mix =="R11" & Datamixes$Round =="Combi4"] <- SummaryBCE_ByExp$BCE.13dpi.Exp[SummaryBCE_ByExp$Strain_ID =="278" & SummaryBCE_ByExp$Round=="Combi4"]
Datamixes$bestStrain[Datamixes$Mix =="R11" & Datamixes$Round =="Combi4"] = "278"


Datamixes <- Datamixes %>%
  mutate(MixAll = Strain_ID.new) %>%
  separate(MixAll, into = c("MixAll", NA, NA), sep ="-")

write.table(Datamixes, "data/AugmentedData/SupplementaryTableMixes_BCEvssingle_corrected_20210709.txt", sep = "\t", row.names=F)

colorsMix <- setNames(c("blue", "red", "orange", "purple", "magenta", "grey", "grey", "grey", "grey"), nm = c("M10.35", "M10.19", "M10.21", "M10.48", "M10.38",
                                                                                                     "M10.22", "M10.43", "M10.50", "M10.8"))


arrowdata <- data.frame(Mix = c("M10.19", "M10.38", "M10.48", "M10.35", "M10.21"), x.from = NA, x.to = NA, y.from = NA, y.to=NA)
for(i in seq_along(1:nrow(arrowdata))){
  arrowdata$y.from[i] = Datamixes$BCE.13dpi.Exp[Datamixes$Strain_ID.new == as.character(arrowdata$Mix[i]) & Datamixes$Round=="Combi4"]
  arrowdata$x.from[i] = Datamixes$meanBCE.bestStrain[Datamixes$Strain_ID.new == as.character(arrowdata$Mix[i]) & Datamixes$Round=="Combi4"]
  arrowdata$y.to[i] = Datamixes$BCE.13dpi.Exp[grepl(as.character(arrowdata$Mix[i]), Datamixes$Strain_ID.new) & Datamixes$Round=="Combi4" & Datamixes$Group =="reduced"]
  arrowdata$x.to[i] = Datamixes$meanBCE.bestStrain[grepl(as.character(arrowdata$Mix[i]), Datamixes$Strain_ID.new) & Datamixes$Round=="Combi4" & Datamixes$Group =="reduced"]
}

Figure4b <- ggplot(Datamixes[Datamixes$Round =="Combi4" & Datamixes$MixAll %in% c("M10.19", "M10.38", "M10.48", "M10.35", "M10.21"),], aes(x = meanBCE.bestStrain, y = BCE)) +
  geom_point(aes(shape = Group, color = MixAll),alpha = 1, size= 2)+
  theme(panel.background = element_blank(), panel.grid= element_line(colour="grey92"),
        panel.grid.minor=element_blank(), panel.grid.major.x = element_blank(), panel.border=element_blank())+
  theme(legend.background = element_rect(fill="white", colour=NA), legend.key=element_rect(fill = "white", colour = NA),
        legend.position = "right")+
  scale_color_manual(values = colorsMix, name = "SynCom")+
  scale_x_continuous("best single strain protection score [a.u.]", limits = c(0, 100))+
  scale_y_continuous("SynCom protection score [a.u.]", limits = c(0,100))+
  theme(axis.text.x = element_text(angle=0, hjust=0.5, vjust=0.5), plot.title = element_text(size=9)) +
  geom_abline(color = "lightgrey")+
   geom_segment(data = arrowdata, aes( x=x.from, y=y.from, xend=x.to, yend=y.to, color = Mix ),
                 lineend="round",arrow=arrow(length=unit(0.1, "inches")),
               alpha=0.2, size=0.8)+
  theme(legend.position = "right")+
  coord_fixed()

saveRDS(Figure4b, "output/M10_Figure4/Figure4b.RDS")
ggsave(filename="FiguresManuscript/M10_M34/Combi4_replacement_20210709.pdf", useDingbats=F, height=6, width=8, scale=0.6)



Fig4ab_combined <- ggarrange(Figure4a, Figure4b, ncol=2, widths = c(1, 1.3),align="h")

ggsave(filename="FiguresManuscript/revision/Figure4ab_combined_20210709.pdf", useDingbats=F, height=4.5, width=12, scale=0.6, units="in")
